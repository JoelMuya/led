void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  delay(9600);
  pinMode(1,OUTPUT);
  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
  delay(1000);

}

void loop() {
  // put your main code here, to run repeatedly:
  delay(5000);
  digitalWrite(1,HIGH);
  digitalWrite(2,LOW);
  digitalWrite(3,LOW);
  if(1==HIGH){
    Serial.println("Red");
  }else{
    Serial.print(" ");
  }
  
  delay(5000);

  digitalWrite(1,LOW);
  digitalWrite(2,HIGH);
  digitalWrite(3,LOW);
  if(2==HIGH){
    Serial.println("Green");
  }else{
    Serial.print(" ");
  }
  
  delay(5000);

  digitalWrite(1,LOW);
  digitalWrite(2,LOW);
  digitalWrite(3,HIGH);
  if(3==HIGH){
    Serial.println("Blue");
  }else{
    Serial.print(" ");
  }
  
  delay(5000);
  

}
